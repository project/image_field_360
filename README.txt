= 360° photo =

  This module creates a field formatter that lets users expirence a 360° image.


== Installation ==

  To install the 360° Image module, extract the module to your
  modules folder, such as sites/all/modules. Bevore using the module the 
  required library must be installed. Download it from 
  https://github.com/JeremyHeleine/Photo-Sphere-Viewer and extract the contents
  to a folder called 'photo_sphere_viewer' inside your libraries folder, such as 
  sites/all/libraries/photo_sphere_viewer. In order to function correctly 
  at least the files 'photo-sphere-viewer.min.js' and 'three.min.js' are 
  required inside the 'photo_sphere_viewer' folder.
  To check if the librarie was installed correctly go to "admin/reports/status"
  and look for 360° Image.


== Use ==

  To use the module create or use an existing content type with an image field,
  go to the manage display section and select "360 degree view" as format for 
  the image field. Finally choose the settings you want.
  Warning: Pictures that are not 360×180 degrees panoramas may look strange.
